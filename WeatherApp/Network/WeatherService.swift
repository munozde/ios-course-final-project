//
//  WeatherService.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/13/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import ObjectMapper

typealias WeatherSequence = PrimitiveSequence<SingleTrait, CurrentWeather?>

class WeatherService {
    
    private let provider = MoyaProvider<NetworkManagerProvider>()
    
    func getCurrentWeather(lat: Double, lon: Double) -> WeatherSequence {
        // Option + click to see the (return) type "PrimitiveSequence<SingleTrait, PokemonMapper?>"
        return provider.rx.request(.getCurrentWeather(lat: lat, lon: lon))
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .map { weather in
                return Mapper<CurrentWeather>().map(JSONObject: weather)
        }
    }
    
    func getCurrentWeatherByCity(city: String) -> WeatherSequence {
        return provider.rx.request(.getCurrentWeatherByCity(city: city))
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .map { weather in
                return Mapper<CurrentWeather>().map(JSONObject: weather)
        }
    }
    
    func getForecast(lat: Double, lon: Double) -> PrimitiveSequence<SingleTrait, Forecast?> {
        return provider.rx.request(.getForecast(lat: lat, lon: lon))
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .map { weather in
                return Mapper<Forecast>().map(JSONObject: weather)
                
        }
    }
    
    func getWeatherIcon(iconId: String) -> PrimitiveSequence<SingleTrait, UIImage?> {
        return provider.rx.request(.getWeatherIcon(iconId: iconId))
            .filterSuccessfulStatusCodes()
            .mapImage()
            .map { image in
                return image
        }
    }
    
}
