//
//  WeatherServiceProvider.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/13/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import Foundation
import Moya

let API_KEY = "eb5051ae618e7f7701d8010cdee43c9f";
enum NetworkManagerProvider {
    case getCurrentWeather(lat: Double, lon: Double)
    case getCurrentWeatherByCity(city: String)
    case getForecast(lat: Double, lon: Double)
    case getWeatherIcon(iconId: String)
}

extension NetworkManagerProvider: TargetType {
    var baseURL: URL {
        switch self {
        case .getCurrentWeather(_), .getCurrentWeatherByCity(_), .getForecast(_):
                return URL(string: "https://api.openweathermap.org/data/2.5/")!
        case .getWeatherIcon(_):
            return URL(string: "https://openweathermap.org/img/w/")!
        }
    }
    
    var path: String {
        switch self {
        case .getCurrentWeather(_), .getCurrentWeatherByCity(_):
            return "weather"
        case .getForecast(_):
            return "forecast"
        case .getWeatherIcon(let icon):
            return "\(icon).png"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getCurrentWeather(_), .getCurrentWeatherByCity(_), .getForecast(_):
            return .get
        case .getWeatherIcon(_):
            return .get;
        }
        
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getCurrentWeather(let lat, let lon):
            return .requestParameters(parameters: ["APPID" : API_KEY, "lat": lat, "lon": lon], encoding:
                URLEncoding.queryString)
        case .getCurrentWeatherByCity(let city):
            return .requestParameters(parameters: ["APPID" : API_KEY, "q": city], encoding:
                URLEncoding.queryString)
        case .getForecast(lat: let lat, let lon):
            return .requestParameters(parameters: ["APPID" : API_KEY, "lat": lat, "lon": lon], encoding:
                URLEncoding.queryString)
        case .getWeatherIcon(_):
            return .requestPlain
        }
        
    }

    var headers: [String : String]? {
        return [:]
    }

}
