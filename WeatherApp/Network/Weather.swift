//
//  Weather.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/13/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import Foundation
import ObjectMapper

class CurrentWeather: Mappable {
    var main: Main?
    var dt: Int?
    var dtTxt: String?
    var weather: [Weather]?
    var name: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name <- map["name"]
        dt <- map["dt"]
        dtTxt <- map["dt_txt"]
        main <- map["main"]
        weather <- map["weather"]
    }
}

class Main: Mappable {
    var temp: Double?
    
    required init?(map: Map) {}
    
    func mapping(map: Map){
        temp <- map["temp"]
    }
}

class Weather: Mappable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        main <- map["main"]
        description <- map["description"]
        icon <- map["icon"]
    }
}

class Forecast: Mappable {
    var list: [CurrentWeather]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        list <- map["list"]
    }
}
