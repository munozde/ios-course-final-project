//
//  SettingsRealm.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/15/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import Foundation
import RealmSwift

class SettingRealm: Object {
    @objc dynamic var key: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var maxNumberOfDays: Int = 5

    convenience init(key: String, city: String, maxNumberOfDays: Int) {
        self.init()
        self.key = key;
        self.city = city;
        self.maxNumberOfDays = maxNumberOfDays
    }
}
