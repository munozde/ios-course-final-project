//
//  SearchRealm.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/16/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import Foundation
import RealmSwift

class SearchRealm: Object {
    @objc dynamic var city: String = ""
    @objc dynamic var weatherTimestamp: Int = 0
    @objc dynamic var weatherMainDescription: String = ""
    @objc dynamic var searchedAt: Int = 0

    convenience init(city: String, weatherTimestamp: Int, weatherMainDescription: String, searchedAt: Int) {
        self.init()
        self.city = city;
        self.weatherTimestamp = weatherTimestamp;
        self.weatherMainDescription = weatherMainDescription
        self.searchedAt = searchedAt
    }
}

