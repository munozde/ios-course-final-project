//
//  RealmHelper.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/15/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import Foundation
import RealmSwift

class RealmHelper {
    
    static var realm = try! Realm()
    
    static func getSettingByCityName(cityName: String) -> SettingRealm? {
        return RealmHelper.getSettingByKey(key: cityName.lowercased())
    }
    
    static func getSettingByKey(key: String) -> SettingRealm? {
        return RealmHelper.realm.objects(SettingRealm.self).filter("key == \"\(key)\"").first
    }
    
    static func saveSearch(search: SearchRealm) throws {
        try RealmHelper.realm.write {
            RealmHelper.realm.add(search)
        }
    }
    
}
