//
//  WeatherViewModel.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/13/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import Foundation

import RxSwift
import RxDataSources

class WeatherViewModel {
    
    let weatherService = WeatherService()
    let currentWeather = PublishSubject<CurrentWeather>()
    let currentForecast = PublishSubject<Forecast>()
    let weatherImage = PublishSubject<UIImage>()
    let forecastWeather: PublishSubject<[CurrentWeather]> = PublishSubject<[CurrentWeather]>()
    let sections = BehaviorSubject<[SectionModel<String, CurrentWeather>]>.init(value: [])
    
    let bag = DisposeBag()
    
    func getCurrentWeather(lat: Double, lon: Double) {
        weatherService.getCurrentWeather(lat: lat, lon: lon).subscribe { (event) in
            switch event {
            case .success(let weather):
                self.currentWeather.onNext(weather!)
                break;
            case .error(let error):
                print(error.localizedDescription)
                break;
            }
            }.disposed(by: bag)
    }
    
    func getCurrentWeatherByCity(city: String) {
        weatherService.getCurrentWeatherByCity(city: city).subscribe { (event) in
            switch event {
            case .success(let weather):
                self.currentWeather.onNext(weather!)
                break;
            case .error(let error):
                print(error.localizedDescription)
                break;
            }
            }.disposed(by: bag)
    }
    
    func getForecast(lat: Double, lon: Double, maxNumberOfDays: Int) {
        weatherService.getForecast(lat: lat, lon: lon).subscribe { (event) in
            switch event {
            case .success(let forecast):
                self.currentForecast.onNext(forecast!)
                self.forecastWeather.onNext(forecast?.list ?? [])
                
                if let list = forecast?.list {
                    var forecastMap : [String: [CurrentWeather]] = [:];
                    var forecastSectionModel : [SectionModel<String, CurrentWeather>] = [];
                    for weather in list {
                        if let timestamp = weather.dt {
                            // TODO get timezone from system...
                            let strDate = DateHelper.formatTimestamp(timestamp: timestamp, timezone: TimeZone.current.identifier, format: "yyyy-MM-dd (EEEE)")
                            
                            if forecastMap[strDate] == nil && forecastMap.count >= maxNumberOfDays {
                                continue;
                            }
                            if forecastMap[strDate] == nil {
                                forecastMap[strDate] = []
                            }
                            forecastMap[strDate]?.append(weather)
                        }
                    }
                    
                    for key in forecastMap.keys.sorted() {
                            forecastSectionModel.append(SectionModel(model: key, items: forecastMap[key] ?? []))
                    }
                    
                    self.sections.onNext(forecastSectionModel)
                }
                break;
            case .error(let error):
                print(error.localizedDescription)
                break;
            }
            }.disposed(by: bag)
        
    }
    
    func getWeatherIcon(iconId: String) {
        weatherService.getWeatherIcon(iconId: iconId).subscribe { (event) in
            switch event {
            case .success(let image):
                self.weatherImage.onNext(image!)
                break;
            case .error(let error):
                print(error.localizedDescription)
                break;
            }
            }.disposed(by: bag)
        
    }
}
