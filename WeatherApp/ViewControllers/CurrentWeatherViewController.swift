//
//  CurrentWeatherViewController.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/13/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import CoreLocation

class CurrentWeatherViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var forecastTableView: UITableView!
    
    let weatherVM = WeatherViewModel()
    let bag = DisposeBag()
    
    var locationManager: CLLocationManager?
    var locationReceived = false;
    var userLocation: CLLocation?;
    var maxNumberOfDays = -1;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add to pList the
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager?.requestWhenInUseAuthorization()
            locationManager?.startUpdatingLocation()
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager?.startUpdatingLocation()
        }
        bindViewModel();
    }
    
    override func viewWillAppear(_ animated: Bool) {        
        let cityName = cityLabel.text!
        if cityName.count == 0 {
            return
        }
        
        if let setting = RealmHelper.getSettingByCityName(cityName: cityName) {
            if self.maxNumberOfDays != setting.maxNumberOfDays {
                weatherVM.getCurrentWeather(lat: userLocation?.coordinate.latitude ?? 0, lon: userLocation?.coordinate.longitude ?? 0)
            }
        }
    }
    
    func bindViewModel() {
        weatherVM.currentWeather.subscribe { weatherEvent in
            // TODO take this value from database if possible considering the city
            let currentWeather = weatherEvent.element!
            let cityName = currentWeather.name ?? ""
            self.maxNumberOfDays = 5;
            if let setting = RealmHelper.getSettingByCityName(cityName: cityName) {
                self.maxNumberOfDays = setting.maxNumberOfDays
            }            
            
            if let iconId = currentWeather.weather?[0].icon {
                self.weatherVM.getWeatherIcon(iconId: iconId)
                self.weatherVM.getForecast(lat: self.userLocation?.coordinate.latitude ?? 0, lon: self.userLocation?.coordinate.longitude ?? 0, maxNumberOfDays: self.maxNumberOfDays)
            }
            
            if let temp = currentWeather.main?.temp {
                // Kelvin to Celcius
                self.tempLabel.text = String(format: "%.0f", temp - 273.15) + "°"
            } else {
                self.tempLabel.text = ""
            }
            
            if let name = currentWeather.name {
                self.cityLabel.text = name.isEmpty ? "N/A" : name
            } else {
                self.cityLabel.text = "N/A"
            }
            
            }.disposed(by: bag)
        
        weatherVM.weatherImage.subscribe { weatherImage in
            self.weatherImageView.image = weatherImage.element
            }.disposed(by: bag)
        
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, CurrentWeather>>(configureCell: { dataSource, table, indexPath, item in
            let cell = table.dequeueReusableCell(withIdentifier: "weatherCell") as! WeatherTableViewCell
            cell.initFromWeather(currentWeather: item)
            return cell
        })
        
        dataSource.titleForHeaderInSection = { dataSource, index in
            return dataSource.sectionModels[index].model
        }
        
        weatherVM.sections
            .bind(to: forecastTableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations[0] as CLLocation
        weatherVM.getCurrentWeather(lat: userLocation?.coordinate.latitude ?? 0, lon: userLocation?.coordinate.longitude ?? 0)
    }
}
