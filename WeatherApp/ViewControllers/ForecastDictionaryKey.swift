//
//  ForecastDictionaryKey.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/16/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import Foundation

class ForecastDictionaryKey {

    var timestamp: Int?
    var day: String?
    
    init(timestamp: Int, day: String) {
        self.timestamp = timestamp
        self.day = day
    }
}
