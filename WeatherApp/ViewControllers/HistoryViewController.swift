//
//  HistoryViewController.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/16/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import UIKit
import RxSwift

class HistoryViewController: UIViewController {

    @IBOutlet weak var historyTableView: UITableView!
    
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    func bindViewModel() {
        
        let searches = RealmHelper.realm.objects(SearchRealm.self)
       
        Observable.collection(from: searches).bind(to: historyTableView.rx.items) { tableView, indexPath, search in
            
            let cell = UITableViewCell()
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
            cell.textLabel?.text = "\(search.city) \(DateHelper.formatTimestamp(timestamp: search.weatherTimestamp, timezone: TimeZone.current.identifier, format: "EEE dd HH:mm, yyyy")) - \(search.weatherMainDescription)"
            return cell
            }.disposed(by: bag)
    }

}
