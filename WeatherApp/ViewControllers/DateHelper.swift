//
//  DateHelper.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/15/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import Foundation

class DateHelper {
    
    static func formatTimestamp(timestamp: Int, timezone: String, format: String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: timezone)
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
}
