//
//  SettingsViewController.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/15/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var maxNumberOfDaysTextField: UITextField!
    @IBOutlet weak var addCityButton: UIButton!
    
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    func bindViewModel() {
        addCityButton.rx.tap.bind {
            if let cityName = self.cityTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), let maxNumberOfDays = Int(self.maxNumberOfDaysTextField.text!)  {
                if maxNumberOfDays >= 1 && maxNumberOfDays <= 5 && cityName.count > 0 {
                    let key = cityName.lowercased()
                    let setting = SettingRealm(key: key, city: cityName, maxNumberOfDays: maxNumberOfDays)
                    do {

                        let currentSetting = RealmHelper.getSettingByKey(key: key)
                        if currentSetting != nil {
                            try RealmHelper.realm.write {
                                currentSetting?.maxNumberOfDays = maxNumberOfDays
                            }
                        } else {
                            try RealmHelper.realm.write {
                                RealmHelper.realm.add(setting)
                            }
                        }
                        // TODO show a success message
                        self.cityTextField.text = ""
                        self.maxNumberOfDaysTextField.text = ""
                    } catch {
                        print("Error saving setting!")
                    }                    
                } else {
                    print("maxNumberOfDays numeric out of range OR emptyCity!!!")
                }
            } else {
                print("Missing required field!!!")
            }
            
            }.disposed(by: bag)
        
    }
    
}
