//
//  WeatherCell.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/14/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//
import UIKit
import RxSwift

class WeatherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    let weatherVM = WeatherViewModel()
    let bag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initFromWeather(currentWeather: CurrentWeather?) {
        if let timestamp = currentWeather?.dt {
            //            var dateTimeComp = timestamp.components(separatedBy: " ")
            // TODO get timezone from system...
            let time = DateHelper.formatTimestamp(timestamp: timestamp, timezone: TimeZone.current.identifier, format: "HH:mm")
            tempLabel.text = "\(time) \(currentWeather?.weather?[0].main ?? "")"
        } else {
            tempLabel.text = ""
        }
        
        if let iconId = currentWeather?.weather?[0].icon {
            weatherVM.getWeatherIcon(iconId: iconId)
            weatherVM.weatherImage.subscribe { image in
                self.iconImageView.image = image.element
                }.disposed(by: bag)
        }
        
    }
    
}
