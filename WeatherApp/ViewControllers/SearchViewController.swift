//
//  SearchViewController.swift
//  WeatherApp
//
//  Created by Daniel Muñoz on 3/16/19.
//  Copyright © 2019 Daniel Muñoz. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift
import RxRealm

class SearchViewController: UIViewController {
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cityPickerView: UIPickerView!
    
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet weak var goButton: UIButton!
    
    let bag = DisposeBag()
    let weatherVM = WeatherViewModel()
    var modelBinded = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !modelBinded {
            bindViewModel()
        }
    }
    
    func bindViewModel() {
        let settings = RealmHelper.realm.objects(SettingRealm.self)
        if(settings.count == 0) {
            return;
        }
        
        modelBinded = true
        goButton.rx.tap.bind {
            if self.cityTextField?.text?.count  ?? 0 > 0 {
                self.weatherVM.getCurrentWeatherByCity(city: self.cityTextField.text ?? "")
            }
            }.disposed(by: bag)
        
        Observable.collection(from: settings)
            .map({(items) -> [SettingRealm] in
                return items.sorted(by: { (item1, item2) -> Bool in
                    return item1.city < item2.city
                })
            })
            .bind(to: cityPickerView.rx.items) { index, item, _ in
                if index == 0 && self.cityTextField.text?.count == 0 {
                    self.cityPickerView.selectRow(0, inComponent: 0, animated: true)
                    self.cityTextField.text = item.city
                    self.weatherIconImageView.image = nil
                    self.tempLabel.text = ""
                } else if self.cityTextField.text == item.city {
                    self.cityPickerView.selectRow(index, inComponent: 0, animated: true)
                }
                
                let cityPickerLabel = UILabel()
                cityPickerLabel.text = item.city
                cityPickerLabel.textAlignment = .center
                return cityPickerLabel
            }.disposed(by: bag)
        
        cityPickerView.rx.modelSelected(SettingRealm.self)
            .subscribe(onNext: { settings in
                self.cityTextField.text = settings[0].city
                self.tempLabel.text = ""
                self.weatherIconImageView.image
                    = nil
            })
            .disposed(by: bag)
        
        weatherVM.currentWeather.subscribe { weatherEvent in
            let currentWeather = weatherEvent.element!
            if let iconId = currentWeather.weather?[0].icon {
                self.weatherVM.getWeatherIcon(iconId: iconId)
            }
            
            if let temp = currentWeather.main?.temp {
                let celciusTemp =  temp - 273.15
                self.tempLabel.text = String(format: "%.0f", celciusTemp) + "°"
                let search = SearchRealm(city: self.cityTextField.text!, weatherTimestamp: weatherEvent.element?.dt ?? 0, weatherMainDescription: currentWeather.weather?[0].main ?? "", searchedAt: Int(NSDate().timeIntervalSince1970))
                
                do {
                    try RealmHelper.saveSearch(search: search)
                } catch {
                    print("Horror saving search...", error)
                }
            } else {
                self.tempLabel.text = ""
            }
            
            }.disposed(by: bag)
        
        weatherVM.weatherImage.subscribe { weatherImage in
            self.weatherIconImageView.image = weatherImage.element
            }.disposed(by: bag)
    }
    
}
